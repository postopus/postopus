{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Units"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "Internally, octopus uses atomic units for computing values. Nonetheless, the user can choose the output units with the `inp` parameter [UnitsOutput](https://octopus-code.org/documentation/main/variables/execution/units/unitsoutput/). The default is `atomic` [units], but one can use `ev_angstrom` as well. In the latter case, the spatial dimensions will be measured in Angstrom and the energies will be measured in `eV`. In the `atomic` case, the spatial dimensions will be measured in bohr, while all the other variables will be measured in atomic units. More on this in the [octopus documentation](https://octopus-code.org/documentation/main/variables/execution/units/). The election of the `inp` parameter of the user is reflected in the `parser.log` after the run is finished. We retrieve the unit information from there.\n",
    "\n",
    "We will compare the benzene and methane test examples.\n",
    "\n",
    "Note: We will assume that you already know from other tutorials that defining the input file and running octopus in the same notebook is recommended. We will not do it here, we just assume the data exsists in the right folder."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "## Methane example (atomic)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "For the methane example, we've left the default atomic units, internally Octopus interprets it as a 0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd ../octopus_data/methane/exec"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 100 parser.log | grep \"UnitsOutput\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "We load the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "from postopus import Run\n",
    "import holoviews as hv\n",
    "from holoviews import opts  # For setting defaults\n",
    "\n",
    "hv.extension(\"bokeh\", \"matplotlib\")  # Allow for interactive plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd ../../"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd methane"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa = run.default.scf.density(\"ncdf\").isel(step=-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "Here, we can see that the `xarray` has the units `au`. In the following we will show how to access these units and also the units of the individual coordinates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa.y.units"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "If we use holoviews for plotting, the coordinates are automatically labeled according to the units of the `xarray`. Unfortunately, labeling the color bar needs to be done manually. For more information about plotting with holoviews, look at the [holoviews]((holoviews_with_postopus.ipynb) tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "hv_ds = hv.Dataset(xa)\n",
    "hv_im = hv_ds.to(hv.Image, kdims=[\"x\", \"y\"], dynamic=True)\n",
    "hv_im.opts(\n",
    "    colorbar=True,\n",
    "    width=500,\n",
    "    height=400,\n",
    "    cmap=\"seismic\",\n",
    "    clabel=f\"{xa.name} ({xa.units})\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Benzene example (ev_angstrom)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "For the benzene example, we specified `UnitsOutput = ev_angstrom` in the `inp` file. This is interpreted as the number 1 internally in Octopus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd ../benzene/exec"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 100 parser.log | grep \"UnitsOutput\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd .."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "runb = Run(\".\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": [
    "We will do the same exploration as we did before with methane:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "xab = runb.default.scf.density(source=\"ncdf\").isel(step=-1)\n",
    "xab"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "xab.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "xab.x.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27",
   "metadata": {},
   "outputs": [],
   "source": [
    "hv_dsb = hv.Dataset(xab)\n",
    "hv_imb = hv_dsb.to(hv.Image, kdims=[\"x\", \"y\"])\n",
    "hv_imb.opts(\n",
    "    colorbar=True,\n",
    "    width=500,\n",
    "    height=400,\n",
    "    cmap=\"seismic\",\n",
    "    clabel=f\"{xab.name} ({xab.units})\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "## Cube format as the exception"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29",
   "metadata": {},
   "source": [
    "The only (known) exception to this rule is the `cube` format. The format itself [specifies that the values should be calculated in atomic units](https://paulbourke.net/dataformats/cube/) (if the number of voxels in a dimension is positive, which to the best of our knowledge, is always the case in Octopus 13.0). Thus, the `cube` output will always be in bohr, independently of what the `UnitsOutput` parameter says.\n",
    "Actually, `xcrysden` has also an [analogous specification](http://www.xcrysden.org/doc/XSF.html) but is ignored by octopus for now. So, we will read `xcrysden` with the unit that the user specifies in the `inp`, but this [may change in the future](https://gitlab.com/octopus-code/octopus/-/issues/592)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa_cube = runb.default.scf.density(source=\"cube\")\n",
    "xa_cube"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa_cube.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa_cube.x.units"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33",
   "metadata": {},
   "source": [
    "We will clearly see that the scaling is different in comparison to the last plot, due to the change of units:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34",
   "metadata": {},
   "outputs": [],
   "source": [
    "hv_ds_cube = hv.Dataset(xa_cube)\n",
    "hv_im_cube = hv_ds_cube.to(hv.Image, kdims=[\"x\", \"y\"])\n",
    "hv_im_cube.opts(\n",
    "    colorbar=True,\n",
    "    width=500,\n",
    "    height=400,\n",
    "    cmap=\"seismic\",\n",
    "    clabel=f\"{xa_cube.name} ({xa_cube.units})\",\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
