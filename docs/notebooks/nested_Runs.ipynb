{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Nested Runs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "In this tutorial, we will explore the `nestedRuns` module from the postopus package, which allows you to analyze multiple runs at once. This feature is useful when working with a large number of simulation runs, making it easier to access and process data from multiple runs simultaneously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "from postopus import nestedRuns\n",
    "from pathlib import Path\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "input file is already defined in the folder (s. GitLab repo), otherwise we recommend defining it in the notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd ../octopus_data/nested_runs/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "For this example, we need to trigger Octopus from a python script."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "!python3 create_runs.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "The initialization of a `nestedRun` object is very similar to the one of a `Run` object. As an argument we need to pass a `Pathlib.Path` of the folder that contains the multiple runs. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "n = nestedRuns(Path(\".\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "The `nestedRun` object will contain nested Dictionaries. These reflect the traversing path from the current working directory to the folder with the data (in this case the `nested_runs` folder). In the data folder, we will also see the `nestedObjects` object that contains the initialised `Run` objects that will be used to retrieve the data. To make the structure of the `nestedRun` object as easy as possible you can initialize the `nestedRun` object in the same folder where your data is stored. This will make the data access much easier as we will see below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "To access the data, we will need to traverse the nested dictionary tree structure until we get to the data level sub-tree. We can use the dot notation and tab completion for traversing. Except for the cases where the path contains a dot. The Python interpreter has problems with that. In these cases, we need to use the typical dictionary-attribute-accesing syntax `[\"\"]`. Since we are already in the correct folder we don't need to traverse further."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "nruns = n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "nruns"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "We can extract individual `Run` objects from the original `nestedRun` object and do all the operations that we know from the other tutorials. Note that here we need to use the square brackets syntax because the individual run folders have a dot in the name (e.g. `deltax_0.6`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = nruns[\"deltax_0.6\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.default.scf.density().plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "We also can use the `nestedObjects.apply` method to retrieve data from each of the run objects at the same time. E.g. here we can see all the convergence data of three different `octopus` runs in one multiindex dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "convergence = pd.concat(nruns.apply(lambda run: run.default.scf.convergence()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "convergence"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "Taking this combined dataframe we can produce complex plots with a few lines of code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_parameter_from_path(path):\n",
    "    # this is a hack to get the spacing from the path\n",
    "    return float(path[-3:])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_converged_data(convergence):\n",
    "    # get only the information from the last iteration for each run\n",
    "    converged = convergence.groupby(level=0).tail(1).droplevel(1)\n",
    "    i = converged.index\n",
    "    combined = converged.set_index(i.map(get_parameter_from_path)).sort_index()\n",
    "    return combined"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "converged = get_converged_data(convergence)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "converged"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "width = 5\n",
    "f, ax = plt.subplots(1, 1, figsize=(width, width * 0.6), sharex=True)\n",
    "ax.plot(converged.index, converged.energy)\n",
    "ax.set_ylabel(\"Total energy [eV]\")\n",
    "ax.set_xlabel(r\"Spacing [$\\AA$]\")\n",
    "f.tight_layout()\n",
    "f.savefig(\"convergence.png\")\n",
    "\n",
    "f, ax = plt.subplots(1, 1, figsize=(width, width * 0.6), sharex=True)\n",
    "for k, group in convergence.groupby(level=0):\n",
    "    ax.semilogy(\n",
    "        group.loc[k].index,\n",
    "        group.rel_dens,\n",
    "        label=rf\"Spacing {get_parameter_from_path(k)} $\\AA$\",\n",
    "    )\n",
    "ax.legend()\n",
    "ax.set_ylabel(\"Relative density change\")\n",
    "ax.set_xlabel(r\"Iteration number\")\n",
    "f.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "We can also retrieve field data in an analogous manner."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28",
   "metadata": {},
   "outputs": [],
   "source": [
    "nruns.apply(lambda run: run.default.scf.density())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
