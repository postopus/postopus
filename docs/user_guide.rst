User Guide
----------

.. toctree::
   :maxdepth: 1

   notebooks/Detailed_Overview.ipynb
   notebooks/xarray-plots1.ipynb
   notebooks/holoviews_with_postopus
   notebooks/xrft.ipynb
   notebooks/units.ipynb
   notebooks/nested_Runs.ipynb
