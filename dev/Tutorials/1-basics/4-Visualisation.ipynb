{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualization\n",
    "[Link to tutorial](https://octopus-code.org/documentation/13/tutorial/basics/visualization/)\n",
    "\n",
    "In this tutorial we will explain how to visualize outputs from **Octopus**. Several different file formats are available in **Octopus** that are suitable for a variety of visualization software. We will not cover all of them here. See [Visualization](https://www.octopus-code.org/documentation//13/manual/visualization) for more information.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 4-visualisation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 4-visualisation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Benzene molecule\n",
    "\n",
    "As an example, we will use the benzene molecule. At this point, the input should be quite familiar to you:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_gs.txt\"\n",
    "stderr = \"stderr_gs.txt\"\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_Angstrom\n",
    "\n",
    "Radius = 5*angstrom\n",
    "Spacing = 0.15*angstrom\n",
    "\n",
    "%Output\n",
    "  wfs\n",
    "  density\n",
    "  elf\n",
    "  potential\n",
    "%\n",
    "\n",
    "OutputFormat = cube + xcrysden + dx + axis_x + plane_z\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Coordinates are in this case given in the `benzene.xyz` file. This file should look like:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile benzene.xyz\n",
    "\n",
    "\n",
    "12\n",
    "   Geometry of benzene (in Angstrom)\n",
    "   C  0.000  1.396  0.000\n",
    "   C  1.209  0.698  0.000\n",
    "   C  1.209 -0.698  0.000\n",
    "   C  0.000 -1.396  0.000\n",
    "   C -1.209 -0.698  0.000\n",
    "   C -1.209  0.698  0.000\n",
    "   H  0.000  2.479  0.000\n",
    "   H  2.147  1.240  0.000\n",
    "   H  2.147 -1.240  0.000\n",
    "   H  0.000 -2.479  0.000\n",
    "   H -2.147 -1.240  0.000\n",
    "   H -2.147  1.240  0.000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have used two new input variables:\n",
    "\n",
    "* [Output](https://www.octopus-code.org/documentation//13/variables/output/output): this tells **Octopus** what quantities to output.\n",
    "* [OutputFormat](https://www.octopus-code.org/documentation//13/variables/output/outputformat): specifies the format of the output files.\n",
    "\n",
    "In this case we ask **Octopus** to output the wavefunctions (`wfs`), the density, the electron localization function (`elf`) and the Kohn-Sham potential. We ask **Octopus** to generate this output in several formats, that are required for the different visualization tools that we mention below. If you want to save some disk space you can just keep the option that corresponds to the program you will use. Take a look at the documentation on variables [Output](https://www.octopus-code.org/documentation//13/variables/output/output) and [OutputFormat](https://www.octopus-code.org/documentation//13/variables/output/outputformat) for the full list of possible quantities to output and formats to visualize them in.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls static"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualization\n",
    "We use postopus to collect and visualize the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts  # For setting defaults\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from postopus import Run\n",
    "\n",
    "# hv.extension('matplotlib')  # Uncomment to use matplotlib as backend\n",
    "hv.extension(\"bokeh\")  # Allow for interactive plots\n",
    "\n",
    "# Choose color scheme similar to matplotlib\n",
    "opts.defaults(\n",
    "    opts.Image(cmap=\"viridis\", width=500, height=400),\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Run object from the output files\n",
    "run = Run(\".\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot of energy at each interaction showing its convergence\n",
    "fig = run.default.scf.convergence()[\"energy\"].plot(\n",
    "    title=\"Energy convergence\",\n",
    "    marker=\".\",\n",
    "    markersize=10,\n",
    ")\n",
    "fig.set(xlabel=\"Iteration\", ylabel=\"Energy (eV)\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot a slice of the density in the xy plane (z=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot(s1):\n",
    "    fig, ax = plt.subplots()\n",
    "    s1.plot(ax=ax)\n",
    "    ax.set_title(\"Density of benzene in the xy plane (z=0)\")\n",
    "    ax.set_xlabel(\"x (Å)\")\n",
    "    ax.set_ylabel(\"y (Å)\")\n",
    "    return fig, ax"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "density = run.default.scf.density\n",
    "xa = density(source=\"cube\")[-1]  # Get the converged density\n",
    "s1 = xa.sel(z=0, method=\"nearest\")  # Select the xy plane (nearest data point to z=0)\n",
    "fig, ax = plot(s1)  # Plot the density"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot of 6 2D slice of the density along the x axis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(\n",
    "    3, 2, figsize=(16, 18)\n",
    ")  # create a subplot with 2 rows, 3 columns\n",
    "x_positions = np.linspace(-7.5, 5, 6)  # x positions from -7.5 to 5 in 6 steps\n",
    "for ax, x in zip(axs.flat, x_positions):\n",
    "    xa.sel(x=x, method=\"nearest\").plot(\n",
    "        ax=ax, x=\"y\"\n",
    "    )  # plot the slice nearest to x position\n",
    "\n",
    "fig.tight_layout()  # avoid overlap of labels"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot of the electron density in xy plane with an interactive slider to select the z position"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Holoviews\n",
    "hv.Dataset(xa).to(hv.Image, kdims=[\"x\", \"y\"], dynamic=True).opts(\n",
    "    colorbar=True, clim=(0, xa.values.max())\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Go to *5-Centering_Example.ipynb*](5-Centering_Example.ipynb)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  },
  "vscode": {
   "interpreter": {
    "hash": "7d86675be647ed983eca0751a5c5cd6e52cfa67869ea07edc2928c4d9b3ecdee"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
