{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Dispersive linear media\n",
    "[Link to tutorial](https://octopus-code.org/documentation/13/tutorial/maxwell/run04/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.cm as cm\n",
    "import holoviews as hv\n",
    "from postopus import Run\n",
    "\n",
    "hv.extension(\"bokeh\")  # Allow for interactive plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 5-dispersive-linear-objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 5-dispersive-linear-objects"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "In addition to the possibility to include objects described as static linear\n",
    "media, Octopus allows for simulations with dispersive media. For example, let's\n",
    "consider the following input file, for propagation of a pulse through a sphere\n",
    "described by a Drude polarizability:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout.txt'\n",
    "stderr = 'stderr.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "ExperimentalFeatures = yes\n",
    "Restartwalltimeperiod = 1.1\n",
    "%Systems\n",
    " 'Maxwell' | maxwell\n",
    " 'NP' | dispersive_medium\n",
    "%\n",
    "\n",
    " MediumPoleEnergy = 9.03*ev\n",
    " MediumPoleDamping = 0.053*ev\n",
    " MediumDispersionType = drude_medium\n",
    " %MediumCurrentCoordinates\n",
    "   -160.0*nm | 0.0 | 0.0\n",
    "   -80.0*nm | 0.0 | 0.0\n",
    "%\n",
    "# ----- Maxwell box variables ---------------------------------------------------------------------\n",
    "\n",
    " l_zero = 550*nm     #central wavelength\n",
    " lsize_mx = 1.25*l_zero\n",
    " lsize_myz = 0.5*l_zero\n",
    " S_c = 0.2 ##Courant condition coefficient\n",
    "\n",
    " dx_mx    = 20*nm\n",
    " BoxShape   = parallelepiped\n",
    " NP.BoxShape = box_cgal\n",
    " NP.BoxCgalFile = \"gold-np-r80nm.off\"\n",
    "\n",
    "%Lsize\n",
    " lsize_mx+0.25*l_zero | lsize_myz+0.25*l_zero | lsize_myz+0.25*l_zero\n",
    "%\n",
    "\n",
    "%Spacing\n",
    " dx_mx | dx_mx | dx_mx\n",
    "%\n",
    "\n",
    "%MaxwellBoundaryConditions\n",
    " plane_waves | zero | zero\n",
    "%\n",
    "\n",
    "%MaxwellAbsorbingBoundaries\n",
    " cpml | cpml | cpml\n",
    "%\n",
    "\n",
    "MaxwellABWidth              = 0.25*l_zero\n",
    "MaxwellABPMLPower           = 3.0\n",
    "MaxwellABPMLReflectionError = 1e-16\n",
    "\n",
    "OutputFormat = axis_x + plane_y\n",
    "\n",
    "%MaxwellOutput\n",
    " electric_field\n",
    " total_current_mxll\n",
    "%\n",
    "\n",
    "MaxwellOutputInterval = 20\n",
    "MaxwellTDOutput       = maxwell_energy + maxwell_total_e_field\n",
    "\n",
    "%MaxwellFieldsCoordinate\n",
    "  -120.0*nm | 0.0 | 0.0\n",
    "  -80.0*nm | 0.0 | 0.0\n",
    "  -20.0*nm | 0.0 | 0.0\n",
    "%\n",
    "\n",
    "Maxwell.TDSystemPropagator = prop_expmid\n",
    "NP.TDSystemPropagator      = prop_rk4\n",
    "\n",
    "timestep                = S_c*dx_mx/c\n",
    "TDTimeStep              = timestep\n",
    "TDPropagationTime       = 240*timestep\n",
    "\n",
    "lambda = l_zero\n",
    "omega  = 2 * pi * c / lambda\n",
    "kx     = omega / c\n",
    "Ez    = 1.0\n",
    "sigma = 40.0*c\n",
    "p_s     = -lsize_mx*1.2\n",
    "\n",
    "%UserDefinedInitialMaxwellStates\n",
    " use_incident_waves\n",
    "%\n",
    "\n",
    "%MaxwellIncidentWaves\n",
    " plane_wave_mx_function | electric_field | 0 | 0 | Ez | \"plane_waves_function\"\n",
    "%\n",
    "\n",
    "%MaxwellFunctions\n",
    " \"plane_waves_function\" | mxf_gaussian_wave | kx | 0 | 0 | p_s | 0 | 0 | sigma\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "Here, we need to define the variables [MediumPoleEnergy](https://www.octopus-code.org/documentation//13/variables/maxwell/mediumpoleenergy) and\n",
    "[MediumPoleDamping](https://www.octopus-code.org/documentation//13/variables/maxwell/mediumpoledamping), which represent the plasma frequency $\\omega_p$ and\n",
    "inverse lifetime $\\gamma$ of a Drude pole. For this example, the parameters are those\n",
    "for the Drude peak of gold, as taken from the literature [$^1$](#first_reference). Also,\n",
    "we can define [MediumCurrentCoordinates](https://www.octopus-code.org/documentation//13/variables/maxwell/mediumcurrentcoordinates) to obtain the\n",
    "polarization current at certain points. The rest of the input variables of\n",
    "the medium are the same that have been used for the static linear medium. In\n",
    "this case, we are using an off file that contains the shape of a sphere of 80\n",
    "nm radius, which is displaced from the origin by one radius to the negative x\n",
    "direction.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note that the off file is provided with this notebook already.\n",
    "!cp ../5-dispersive-linear-media.off gold-np-r80nm.off"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "For practical reasons, we set the box size as a function of the incoming pulse\n",
    "wavelength `l_zero`, and we make the box larger in the direction of propagation.\n",
    "We also add the spacing needed for the PML boundaries, defined below. As you\n",
    "can see, this is defined as 550 nm, using the default parameter nm to convert\n",
    "it to atomic units. Also, we set a relatively small Courant number, which will\n",
    "replace the $1/\\sqrt(3)$ value that was explained before. This is because for\n",
    "Drude media, the time step is also limited by the plasma frequency of the\n",
    "metal, and not only by the grid spacing, so a larger value will cause the\n",
    "simulation to explode (you are welcome to try and increase it, plot the total\n",
    "integrated energy, and observe the divergence after the threshold). For this\n",
    "setup, 0.1 is an appropriate Courant number.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "After we run the simulation, we can plot again the z-component of the electric\n",
    "field in the xz-plane for three different time steps, using the following script:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "ef = run.Maxwell.td.e_field(\"y=0\").vz\n",
    "boxshape_x = slice(-7500, 7500)\n",
    "boxshape_z = slice(-7500, 7500)\n",
    "box = ef.sel(x=boxshape_x, z=boxshape_z)\n",
    "hv_ds = hv.Dataset(box)\n",
    "hv_im = hv_ds.to(hv.Image, kdims=[\"x\", \"z\"], dynamic=True)\n",
    "hv_im.options(\n",
    "    cmap=\"seismic\",\n",
    "    clim=(ef.min().item(), ef.max().item()),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "As it is expected, as the medium reacts to the external field via its\n",
    "polarization current density, screening it inside the sphere, as it is expected\n",
    "from a metal. We can also note that the space discretization used is a rather\n",
    "coarse mesh in this tutorial, due to computation time, but the outcomes are\n",
    "still reasonable.\n",
    "\n",
    "Finally, we can examine how these currents arise in time. Using the following\n",
    "script we can plot the current at three different points (the ones we requested\n",
    "in the input file, namely: near the surface of the sphere towards the negative\n",
    "x axis, in the middle, and near the surface on the positive x axis). Also we\n",
    "plot the E field at these points.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "current_at_points = run.NP.td.current_at_points()\n",
    "ax = current_at_points.plot(x=\"t\", y=[\"j(1,3)\", \"j(2,3)\"], label=[\"before\", \"middle\"])\n",
    "ax.set_ylabel(\"$J_z$\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "E_z = run.Maxwell.td.total_e_field().z\n",
    "ax = E_z.plot(\n",
    "    x=\"t\",\n",
    "    y=[\n",
    "        \"E(1)\",\n",
    "        \"E(2)\",\n",
    "        \"E(3)\",\n",
    "    ],\n",
    "    label=[\"before\", \"middle\", \"after\"],\n",
    ")\n",
    "ax.set_xlabel(\"time step\")\n",
    "ax.set_ylabel(\"$E_z$\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "As can be seen, the current in the direction of the incident electric field\n",
    "(called \"before\") is larger, screening the field effectively. This can be seen\n",
    "by plotting the electric field in the other points, where the values in the\n",
    "\"middle\" and \"after\" are much smaller than \"before\" (actually, the plotted\n",
    "field has already been quenched by the medium, otherwise the waveform would be\n",
    "that of the Gaussian envelope used). In addition to scattering, there is\n",
    "absorption of energy by the nanoparticle, given by the imaginary part of the\n",
    "polarizability. Using a setup like this, and processing the proper values of\n",
    "the EM field in full space and time, it would be possible to calculate an\n",
    "extinction spectrum of the sphere, which can be compared to the one calculated\n",
    "using Mie theory, or other methods.\n",
    "\n",
    "\n",
    "[Back to main tutorial series](../Readme.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "## References\n",
    "\n",
    "1. Aleksandar D. Rakić, Aleksandra B. Djurišić, Jovan M. Elazar, and Marian L. Majewski, Optical properties of metallic films for vertical-cavity optoelectronic devices, [Applied Optics](https://doi.org/10.1364/AO.37.005271) 37 5271 (1998);\n",
    "<span id=\"first_reference\"></span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
