{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Optical spectra from casida\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/response/optical_spectra_from_casida/)\n",
    "\n",
    "In this tutorial we will again calculate the absorption spectrum of methane, but this time using Casida's equations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 3_Optical_spectra_from_casida"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 3_Optical_spectra_from_casida"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Ground-state\n",
    "\n",
    "Once again our first step will be the calculation of the ground state. We will use the following input file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "Note that we are using the values for the spacing and radius that were found in the [Convergence_of_the_optical_spectra tutorial](2_Convergence_of_optical_spectra.ipynb) to converge the absorption spectrum.\n",
    "\n",
    "## Unoccupied States\n",
    "\n",
    "The Casida equation is a (pseudo-)eigenvalue equation written in the basis of particle-hole states. This means that we need both the occupied states -- computed in the ground-state calculation -- as well as the unoccupied states, that we will now obtain, via a non-self-consistent calculation using the density computed in `gs`. The input file we will use is\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_unocc.txt'\n",
    "stderr = 'stderr_unocc.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "ExtraStates = 10"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "Here we have changed the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) to `gs` and added 10 extra states by setting the [ExtraStates](https://www.octopus-code.org/documentation//13/variables/states/extrastates) input variable.\n",
    "\n",
    "By running **Octopus**, we will obtain the first 10 unoccupied states. The solution of the unoccupied states is controlled by the variables in section [Eigensolver](https://www.octopus-code.org/documentation//13/variables/scf/eigensolver/eigensolver). You can take a look at the eigenvalues of the unoccupied states in the file `static/eigenvalues`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "run.default.scf.eigenvalues()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "##  Casida calculation\n",
    "\n",
    "Now modify the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) to `gs` and rerun **Octopus**. Note that by default **Octopus** will use all occupied and unoccupied states that it has available.\n",
    "\n",
    "Sometimes, it is useful not to use all states. For example, if you have a molecule with 200 atoms and 400 occupied states ranging from -50 to -2 eV, and you are interested in looking at excitations in the visible, you can try to use only the states that are within 10 eV from the Fermi energy. You could select the states to use with [CasidaKohnShamStates](https://www.octopus-code.org/documentation//13/variables/linear_response/casida/casidakohnshamstates) or [CasidaKSEnergyWindow](https://www.octopus-code.org/documentation//13/variables/linear_response/casida/casidaksenergywindow).\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_casida.txt'\n",
    "stderr = 'stderr_casida.txt'\n",
    "\n",
    "CalculationMode = casida\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "ExtraStates = 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "A new directory will appear named `casida`, where you can find the file `casida/casida`:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat casida/casida"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "The &lt;x&gt;, &lt;y&gt; and &lt;z&gt; are the transition dipole moments:\n",
    "\n",
    "$$\n",
    "  \\langle x \\rangle = \\langle \\Phi\\_0|x|\\Phi_I \\rangle\n",
    "  \\,;\\qquad\n",
    "  \\langle y \\rangle = \\langle \\Phi\\_0|y|\\Phi_I \\rangle\n",
    "  \\,;\\qquad\n",
    "  \\langle z \\rangle = \\langle \\Phi\\_0|z|\\Phi_I \\rangle\n",
    "$$\n",
    "\n",
    "where $\\Phi_0$ is the ground state and $\\Phi_I$ is the given excitation. The\n",
    "oscillator strength is given by:\n",
    "\n",
    "$$\n",
    "  f_I = \\frac{2 m_e}{3 \\hbar^2} \\omega_I \\sum_{n\\in x,y,z} |\\langle\\Phi\\_0|n|\\Phi_I \\rangle |^2\n",
    "$$\n",
    "\n",
    "as the average over the three directions. The optical absorption spectrum can be given as the \"strength function\",\n",
    "which is\n",
    "\n",
    "$$\n",
    "  S(\\omega) = \\sum_I f_I \\delta(\\omega-\\omega_I)\\\\,\n",
    "$$\n",
    "\n",
    "Note that the excitations are degenerate with degeneracy 3. This could already be expected from the $T_d$ symmetry of methane.\n",
    "\n",
    "{{% notice note %}}\n",
    "Note that within the degenerate subspaces, there is some arbitrariness (possibly dependent on the details of your compilation and machine) in the linear combinations of transitions. Therefore, you should not be concerned if you do not have the same results for the components of the transition dipole moments (above) and analysis of the excitations (below). However, the energies and resulting spectra should agree.\n",
    "{{% /notice %}}\n",
    "\n",
    "Further information concerning the excitations can be found in the directory `casida/casida_excitations`. For example, the first excitation at 9.27 eV is analyzed in the file `casida/casida_excitations/00001`:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat casida/casida_excitations/00001"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "These files contain basically the eigenvector of the Casida equation. The first two columns are respectively the index of the occupied and the index of the unoccupied state, the third is the spin index (always 1 when spin-unpolarized), and the fourth is the coefficient of that state in the Casida eigenvector. This eigenvector is normalized to one, so in this case one can say that 74.7% (0.864<sup>2</sup>) of the excitation is from state 3 to state 5 (one of 3 HOMO orbitals->LUMO) with small contribution from some other transitions.\n",
    "\n",
    "##  Absorption spectrum\n",
    "\n",
    "To visualize the spectrum, we need to broaden these delta functions with the utility `oct-casida_spectrum` (run in your working directory, not in `casida`). It convolves the delta functions with Lorentzian functions. The operation of `oct-casida_spectrum` is controlled by the variables [CasidaSpectrumBroadening](https://www.octopus-code.org/documentation//13/variables/utilities/oct-casida_spectrum/casidaspectrumbroadening) (the width of this Lorentzian), [CasidaSpectrumEnergyStep](https://www.octopus-code.org/documentation//13/variables/utilities/oct-casida_spectrum/casidaspectrumenergystep), [CasidaSpectrumMinenergy](https://www.octopus-code.org/documentation//13/variables/utilities/oct-casida_spectrum/casidaspectrumminenergy), and [CasidaSpectrumMaxEnergy](https://www.octopus-code.org/documentation//13/variables/utilities/oct-casida_spectrum/casidaspectrummaxenergy). If you run  `oct-casida_spectrum` you obtain the file `casida/spectrum.casida`. It contains all columns of `casida` broadened.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "Currently oct-utilities are not supported by postopus. This feature will be implemented soon."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-casida_spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td.txt'\n",
    "stderr = 'stderr_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "FromScratch = yes\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.0023/eV\n",
    "TDMaxSteps = 4350  # ~ 10.0/TDTimeStep\n",
    "\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "TDPolarizationDirection = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": [
    "The time required to run the following cell is in the order of 5 to 10 minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-propagation_spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_casida = pd.read_csv(\n",
    "    \"casida/spectrum.casida\",\n",
    "    delimiter=\"  \",\n",
    "    usecols=[0, 4],\n",
    "    names=[\"Energy [eV]\", \"StrengthFunction\"],\n",
    "    skiprows=1,\n",
    "    engine=\"python\",\n",
    ")\n",
    "\n",
    "df_time_propagation = pd.read_csv(\n",
    "    \"cross_section_vector\",\n",
    "    delimiter=\"  \",\n",
    "    usecols=[0, 3],\n",
    "    names=[\"Energy [eV]\", \"StrengthFunction\"],\n",
    "    skiprows=26,\n",
    "    engine=\"python\",\n",
    ")\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.set_title(\"Absorption spectrum\")\n",
    "ax.set_xlabel(\"Energy (eV)\")\n",
    "ax.set_ylabel(\"Strength function (1/eV)\")\n",
    "\n",
    "df_casida.plot(\n",
    "    x=\"Energy [eV]\",\n",
    "    y=\"StrengthFunction\",\n",
    "    ax=ax,\n",
    "    label=\"casida\",\n",
    "    color=\"green\",\n",
    "    xlim=[0, 20],\n",
    ")\n",
    "\n",
    "df_time_propagation.plot(\n",
    "    x=\"Energy [eV]\",\n",
    "    y=\"StrengthFunction\",\n",
    "    ax=ax,\n",
    "    label=\"time-propagation\",\n",
    "    color=\"purple\",\n",
    "    xlim=[0, 20],\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "Comparing the spectrum obtained with the time-propagation in the [Convergence of the optical spectra tutorial](2_Convergence_of_optical_spectra.ipynb) with the one obtained with the Casida approach using the same grid parameters, we can see that\n",
    "\n",
    "* The peaks of the time-propagation are broader. This can be solved by either increasing the total propagation time, or by increasing the broadening in the Casida approach.\n",
    "* The first two peaks are nearly the same. Probably also the third is OK, but the low resolution of the time-propagation does not allow to distinguish the two close peaks that compose it.\n",
    "* For high energies the spectra differ a lot. The reason is that we only used 10 empty states in the Casida approach. In order to describe better this region of the spectrum we would need more. This is why one should always check the convergence of relevant peaks with respect to the number of empty states.\n",
    "\n",
    "You probably noticed that the Casida calculation took much less time than the time-propagation. This is clearly true for small or medium-sized systems. However, the implementation of Casida in **Octopus** has a much worse scaling with the size of the system than the time-propagation, so for larger systems the situation may be different. Note also that in Casida one needs a fair amount of unoccupied states which are fairly difficult to obtain.\n",
    "\n",
    "If you are interested, you may also compare the Casida results against the Kohn-Sham eigenvalue differences in `casida/spectrum.eps_diff` and the Petersilka approximation to Casida in `casida/spectrum.petersilka`.\n",
    "\n",
    "\n",
    "[Go to *4_Optical_spectra_from_Sternheimer.ipynb*](4_Optical_spectra_from_Sternheimer.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
