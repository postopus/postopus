{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Use of symmetries in optical spectra from time-propagation\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/response/use_of_symmetries_in_optical_spectra_from_time-propagation/)\n",
    "\n",
    "In this tutorial we will see how the spatial symmetries of a molecule can be used to reduce the number of time-propagations required to compute the absorption cross-section.\n",
    "\n",
    "## Introduction\n",
    "\n",
    "The dynamic polarizability (related to optical absorption cross-section via $\\sigma = \\frac{4 \\pi \\omega}{c} \\mathrm{Im} \\alpha $) is, in its most general form, a 3x3 tensor. The reason is that we can shine light on the system polarized in any of the three Cartesian axes, and for each of these three cases measure how the dipole of the molecule oscillates along the three Cartesian axes. This usually means that to obtain the full dynamic polarizability of the molecule we usually need to apply 3 different perturbations along $x, y, z\\,$, by setting [TDPolarizationDirection](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationdirection) to 1, 2, or 3.\n",
    "\n",
    "However, if the molecule has some symmetries, it is in general possible to reduce the total number of calculations required to obtain the tensor from 3 to 2, or even 1.[$^1$](#first_reference)\n",
    "\n",
    "To use this formalism in **Octopus**, you need to supply some extra information. The most important thing that the code requires is the information about equivalent axes, that is, directions that are related through some symmetry transformation. Using these axes, we construct a reference frame and specify it with the [TDPolarization](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarization) block. Note that these axes need not be orthogonal, but they must be linearly-independent. The [TDPolarizationEquivAxes](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationequivaxes) tells the code how many equivalent axes there are. Ideally, the reference frame should be chosen to maximize the number of equivalent axes. When using three equivalent axes, an extra input variable, [TDPolarizationWprime](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationwprime), is also required.\n",
    "\n",
    "Let us give a couple of examples, which should make all these concepts easier to understand.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 6_Use_of_symmetries_in_optical_spectra_from_time_propagation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 6_Use_of_symmetries_in_optical_spectra_from_time_propagation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Methane\n",
    "\n",
    "As seen in previous tutorials, the methane molecule has $T_d$ symmetry. This means that it is trivial to find three linearly-independent equivalent axes such that only one time-propagation is needed to obtain the whole tensor. As it happens, we can use the usual $x$, $y$, and $z$ directions, with all of them being equivalent (check the [symmetry operations](https://en.wikipedia.org/wiki/Symmetry_operation) of the $T_d$ [point group](https://en.wikipedia.org/wiki/Molecular_symmetry) if you are not convinced). Therefore we can perform just one propagation with the perturbation along the $x$ direction adding the following to the input file used previously:\n",
    "```\n",
    "\n",
    "%TDPolarization\n",
    " 1 | 0 | 0\n",
    " 0 | 1 | 0\n",
    " 0 | 0 | 1\n",
    "%\n",
    "TDPolarizationDirection = 1\n",
    "TDPolarizationEquivAxes = 3\n",
    "%TDPolarizationWprime\n",
    " 0 | 0 | 1\n",
    "%\n",
    "```\n",
    "\n",
    "#### Groundstate"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "#### Time-propagation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td.txt'\n",
    "stderr = 'stderr_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.004/eV\n",
    "TDMaxSteps = 2500  # ~ 10.0/TDTimeStep\n",
    "\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "\n",
    "%TDPolarization\n",
    " 1 | 0 | 0\n",
    " 0 | 1 | 0\n",
    " 0 | 0 | 1\n",
    "%\n",
    "TDPolarizationDirection = 1\n",
    "TDPolarizationEquivAxes = 3\n",
    "%TDPolarizationWprime\n",
    " 0 | 0 | 1\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "Note that we had omitted the blocks [TDPolarization](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarization) and [TDPolarizationWprime](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationwprime) in the previous tutorials, as these are their default.\n",
    "Once the time-propagation is finished, you will find, as usual, a `td.general/multipoles` file. This time the file contains all the necessary information for **Octopus** to compute the full tensor, so running the `oct-propagation_spectrum` utility will produce two files: `cross_section_vector.1` and `cross_section_tensor`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-propagation_spectrum"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "Try comparing the spectrum for each component of the $\\sigma$ tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv(\n",
    "    \"cross_section_tensor\",\n",
    "    sep=\"\\s{6}|(?<=\\s)-\\s?\",\n",
    "    engine=\"python\",\n",
    "    usecols=[0, 3, 4, 5, 6, 7, 8, 9, 10, 11],\n",
    "    names=[\n",
    "        \"Energy\",\n",
    "        \"sigma(1,1,1)\",\n",
    "        \"sigma(1,2,1)\",\n",
    "        \"sigma(1,3,1)\",\n",
    "        \"sigma(2,1,1)\",\n",
    "        \"sigma(2,2,1)\",\n",
    "        \"sigma(2,3,1)\",\n",
    "        \"sigma(3,1,1)\",\n",
    "        \"sigma(3,2,1)\",\n",
    "        \"sigma(3,3,1)\",\n",
    "    ],\n",
    "    skiprows=13,\n",
    ")\n",
    "\n",
    "df.plot(\n",
    "    x=\"Energy\",\n",
    "    y=[\n",
    "        \"sigma(1,1,1)\",\n",
    "        \"sigma(1,2,1)\",\n",
    "        \"sigma(1,3,1)\",\n",
    "        \"sigma(2,1,1)\",\n",
    "        \"sigma(2,2,1)\",\n",
    "        \"sigma(2,3,1)\",\n",
    "        \"sigma(3,1,1)\",\n",
    "        \"sigma(3,2,1)\",\n",
    "        \"sigma(3,3,1)\",\n",
    "    ],\n",
    "    legend=False,\n",
    "    subplots=True,\n",
    "    layout=(3, 3),\n",
    "    figsize=(12, 12),\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "## Linear molecule\n",
    "\n",
    "Now let us look at a linear molecule. In this case, you might think that we need two calculations to obtain the whole tensor, one for the direction along the axis of the molecule, and another for the axis perpendicular to the molecule. The fact is that we need only one, in a specially chosen direction, so that our field has components both along the axis of the molecule and perpendicular to it. Let us assume that the axis of the molecule is oriented along the $x\\,$ axis. Then we can use\n",
    "\n",
    "```\n",
    " %TDPolarization\n",
    "  1/sqrt(2) | -1/sqrt(2) | 0\n",
    "  1/sqrt(2) |  1/sqrt(2) | 0\n",
    "  1/sqrt(2) |  0         | 1/sqrt(2)\n",
    " %\n",
    " TDPolarizationDirection = 1\n",
    " TDPolarizationEquivAxes = 3\n",
    " %TDPolarizationWprime\n",
    "  1/sqrt(2) |  0         | 1/sqrt(2)\n",
    " %\n",
    "```\n",
    "\n",
    "You should try to convince yourself that the three axes are indeed equivalent and linearly independent. The first and second axes are connected through a simple reflection in the $xz$ plane, transforming the $y$ coordinate from $-1/\\sqrt{2}$ into $1/\\sqrt{2}$. [TDPolarizationWprime](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationwprime) should be set to the result obtained by applying the inverse symmetry operation to the third axis. This actually leaves the third axis unchanged.\n",
    "\n",
    "## Planar molecule\n",
    "\n",
    "Finally, let us look at a general planar molecule (in the $xy$ plane). In principle we need only two calculations (that is reduced to one if more symmetries are present like, ''e.g.'', in benzene). In this case we chose one of the polarization axes on the plane and the other two rotated 45 degrees:\n",
    "\n",
    "<code>%<a href=TODO>TDPolarization</a><br>\n",
    "  1/sqrt(2) | 0 | 1/sqrt(2)<br>\n",
    "  1/sqrt(2) | 0 |-1/sqrt(2)<br>\n",
    "  0         | 1 | 0<br>\n",
    " %<br>\n",
    " <a href=TODO>TDPolarizationEquivAxes</a>\n",
    "</code>\n",
    "\n",
    "\n",
    "In this case, we need two runs, one for [TDPolarizationDirection](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationdirection) equal to 1, and another for it equal to 3. Note that if there are less than 3 equivalent axes, [TDPolarizationWprime](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationwprime) is irrelevant.\n",
    "\n",
    "\n",
    "[Back to main tutorial series](../Readme.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "## References"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "1. M.J.T. Oliveira, A. Castro, M.A.L. Marques, and A. Rubio, On the use of Neumann's principle for the calculation of the polarizability tensor of nanostructures, [J. Nanoscience and Nanotechnology](https://doi.org/10.1166/jnn.2008.142) 8 1-7 (2008);  \n",
    "<span id=\"first_reference\"></span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
