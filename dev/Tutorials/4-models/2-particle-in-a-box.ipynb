{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Particle in a box\n",
    "[Link to tutorial](https://octopus-code.org/documentation/13/tutorial/model/particle_in_a_box/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "In the previous tutorial, we considered applying a user-defined potential. What if we wanted to do the classic quantum problem of a particle in a box, ''i.e.'' an infinite square well?\n",
    "\n",
    "$\n",
    "\\\\\n",
    "V(x)=\n",
    "\\begin{cases}\n",
    " 0, & \\frac{L}{2} < x < \\frac{L}{2} \\cr\n",
    "\\infty, & \\text{otherwise}\n",
    "\\end{cases}\n",
    "$\n",
    "\n",
    "There is no meaningful way to set an infinite value of the potential for a numerical calculation. However, we can instead use the boundary conditions to set up this problem. In the locations where the potential is infinite, the wavefunctions must be zero. Therefore, it is equivalent to solve for an electron in the potential above in all space, or to solve for an electron just in the domain $ x \\in [-\\tfrac{L}{2}, \\tfrac{L}{2}]$ with zero boundary conditions on the edges. In the following input file, we can accomplish this by setting the \"radius\" to $\\tfrac{L}{2}$​, for the default box shape of \"sphere\" which means a line in 1D."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 2-particle-in-a-box"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 2-particle-in-a-box"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "### Input \n",
    "\n",
    "As usual, we will need to write an input file describing the system we want to calculate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "FromScratch = yes\n",
    "CalculationMode = gs\n",
    "\n",
    "Dimensions = 1\n",
    "TheoryLevel = independent_particles\n",
    "\n",
    "L = 10\n",
    "Radius = L/2\n",
    "Spacing = 0.01\n",
    "\n",
    "%Species\n",
    "  \"null\" | species_user_defined | potential_formula | \"0\" | valence | 1\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    "  \"null\" | 0\n",
    "%\n",
    "\n",
    "LCAOStart = lcao_states\n",
    "\n",
    "%Output\n",
    "  wfs\n",
    "%\n",
    "OutputFormat = axis_x"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "Run this input file and look at the ground-state energy and the eigenvalue of the single state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "wf = run.default.scf.wf().isel(step=-1).sel(st=1)\n",
    "plt.plot(wf.x, wf.real)\n",
    "plt.xlim(min(wf.x) - 2, max(wf.x) + 2);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "### Exercises\n",
    "\n",
    "* Calculate unoccupied states and check that they obey the expected relation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_unocc.txt'\n",
    "stderr = 'stderr_unocc.txt'\n",
    "\n",
    "FromScratch = yes\n",
    "CalculationMode = unocc\n",
    "\n",
    "Dimensions = 1\n",
    "TheoryLevel = independent_particles\n",
    "\n",
    "L = 10\n",
    "Radius = L/2\n",
    "Spacing = 0.01\n",
    "\n",
    "%Species\n",
    "  \"null\" | species_user_defined | potential_formula | \"0\" | valence | 1\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    "  \"null\" | 0\n",
    "%\n",
    "\n",
    "LCAOStart = lcao_states\n",
    "extrastates = 10\n",
    "\n",
    "%Output\n",
    "  wfs\n",
    "%\n",
    "OutputFormat = axis_x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Analytical solution\n",
    "def make_psi(n, L):\n",
    "    def psi_n_l(x):\n",
    "        return -((2 / L) ** 0.5) * np.sin(n * np.pi / L * (x + L / 2))\n",
    "\n",
    "    return psi_n_l"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "* Plot the wavefunctions and compare to your expectation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "wfs = run.default.scf.wf().isel(step=-1)\n",
    "states = [1, 2, 4, 6]\n",
    "\n",
    "fig, axs = plt.subplots(len(states))\n",
    "for i, state in enumerate(states):\n",
    "    wf = wfs.sel(st=state).squeeze()\n",
    "    axs[i].plot(wf.x, wf.real, label=f\"$\\Psi_{state}$ (numerical)\")\n",
    "interval = (wfs.x.min().item(), wfs.x.max().item())\n",
    "L = interval[1] - interval[0]\n",
    "x = np.linspace(interval[0], interval[1])\n",
    "for i, n in enumerate(states):\n",
    "    psi_n = make_psi(n, L)\n",
    "    axs[i].plot(x, psi_n(x), label=f\"$\\Psi_{state}$ (analytical)\", linestyle=\"--\")\n",
    "    axs[i].legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "### Further exercises\n",
    "* Vary the box size and check that the energy has the correct dependence.\n",
    "* Set up a calculation of a ‘‘finite’’ square well and compare results to the infinite one as a function of potential step. (Hint: along with the usual arithmetic operations, you may also use logical operators to create a piecewise-defined expression. See [Input file](https://www.octopus-code.org/documentation//13/manual/basics/input_file)).\n",
    "* Try a 2D or 3D infinite square well."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "[Go to *3-1d-helium.ipynb*](3-1d-helium.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
