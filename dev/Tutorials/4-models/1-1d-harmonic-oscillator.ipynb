{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# 1D Harmonic Oscillator\n",
    "[Link to tutorial](https://octopus-code.org/documentation/13/tutorial/model/1d_harmonic_oscillator/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "As a first example we use the standard textbook harmonic oscillator in one dimension and fill it with two non-interacting electrons.\n",
    "\n",
    "### Input\n",
    "\n",
    "The first thing to do is to tell **Octopus** what we want it to do. Write the following lines and save the file as inp."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 1-1d-harmonic-oscillator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 1-1d-harmonic-oscillator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "FromScratch = yes\n",
    "CalculationMode = gs\n",
    "\n",
    "Dimensions = 1\n",
    "TheoryLevel = independent_particles\n",
    "\n",
    "Radius = 10\n",
    "Spacing = 0.1\n",
    "\n",
    "%Species\n",
    "  \"HO\" | species_user_defined | potential_formula | \"0.5*x^2\" | valence | 2\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    "  \"HO\" | 0\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "Most of these input variables should already be familiar. Here is a more detailed explanation for some of the values:\n",
    "\n",
    "* [Dimensions](https://www.octopus-code.org/documentation//13/variables/system/dimensions) = 1: This means that the code should run for 1-dimensional systems. Other options are 2, or 3 (the default). You can actually run in 4D too if you have compiled with the configure flag –max-dim=4.\n",
    "\n",
    "* [TheoryLevel](https://www.octopus-code.org/documentation//13/variables/hamiltonian/theorylevel) = independent_particles: We tell **Octopus** to treat the electrons as non-interacting.\n",
    "\n",
    "* [Radius](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/radius) = 10.0: The radius of the 1D “sphere,” ‘‘i.e.’’ a line; therefore domain extends from -10 to +10 bohr.\n",
    "\n",
    "* %[Species](https://www.octopus-code.org/documentation//13/variables/system/species/species): The species name is “HO”, then the potential formula is given, and finally the number of valence electrons. See [Input file](https://www.octopus-code.org/documentation//13/manual/input_file) for a description of what kind of expressions can be given for the potential formula.\n",
    "\n",
    "* %[Coordinates](https://www.octopus-code.org/documentation//13/variables/system/coordinates/coordinates): add the external potential defined for species “HO” in the position (0,0,0). The coordinates used in the potential formula are relative to this point."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "### Output\n",
    "\n",
    "Now one can execute this file by running Octopus. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "Here are a few things worthy of a closer look in the standard output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "info = run.default.scf.info()\n",
    "total_energy = [lines for lines in info if \"Total       = \" in lines][-1]\n",
    "total_energy = float(total_energy.split(\"=\")[-1])\n",
    "print(total_energy)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "First one finds the listing of the species:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 5 \"[*] Species [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "The potential is $V(x) = 0.5 x^2$, and 5 Hermite-polynomial orbitals are available for LCAO (the number is based on the valence). The theory level is as we requested:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 3 \"[*] Space [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "The electrons are treated as “non-interacting”, which means that the Hartree and exchange-correlation terms are not included. This is usually appropriate for model systems, in particular because the standard XC approximations we use for actual electrons are not correct for “effective electrons” with a different mass."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    " Input: [[MixField](https://www.octopus-code.org/documentation//13/variables/scf/mixing/mixfield) = none] (what to mix during SCF cycles)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "Since we are using independent particles (and only one electron) there is no need to use a mixing scheme to accelerate the SCF convergence. Apart from these differences, the calculation follows the same pattern as in the Getting Started tutorial and converges after a few iterations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 11 \"[*] SCF CYCLE ITER #   28 [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "<div style=\"border: 1px solid black; padding: 10px; background-color: #ADD8E6\">\n",
    "<span style=\"color: blue;\"> Note that, as the electrons are non-interacting, there is actually no self-consistency needed. Nevertheless, Octopus takes several SCF iterations to converge. This is because it takes more than one SCF iteration for the iterative eigensolver to converge the wave-functions and eigenvalues.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "To improve this we can try having a better initial guess for the wave-functions by turning the LCAO on. You can do this by setting [LCAOStart](https://www.octopus-code.org/documentation//13/variables/scf/lcao/lcaostart) = lcao_states – compare how many iterations and matrix-vector products (in total) are required now. Why? (Hint: what are Hermite polynomials?)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "### Exercises\n",
    "\n",
    "The first thing to do is to look at the static/info file. Look at the eigenvalue and total energy. Compare to your expectation from the analytic solution to this problem!\n",
    "\n",
    "We can also play a little bit with the input file and add some other features. For example, what about the other eigenstates of this system? To obtain them we can calculate some unoccupied states. This can be done by changing the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) to\n",
    "\n",
    "[CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) = unocc\n",
    "\n",
    "in the input file. In this mode Octopus will not only give you the occupied states (which contribute to the density) but also the unoccupied ones. Set the number of states with an extra line\n",
    "\n",
    "[ExtraStates](https://www.octopus-code.org/documentation//13/variables/states/extrastates) = 10\n",
    "\n",
    "that will calculate 10 empty states. A thing to note here is that **Octopus** will need the density for this calculation. (Actually for non-interacting electrons the density is not needed for anything, but since **Octopus** is designed for interacting electrons it will try to read the density anyways.) So if you have already performed a static calculation ([CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) = gs) it will just use this result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_unocc.txt'\n",
    "stderr = 'stderr_unocc.txt'\n",
    "\n",
    "FromScratch = yes\n",
    "CalculationMode = unocc\n",
    "\n",
    "Dimensions = 1\n",
    "TheoryLevel = independent_particles\n",
    "\n",
    "Radius = 10\n",
    "Spacing = 0.1\n",
    "\n",
    "%Species\n",
    "  \"HO\" | species_user_defined | potential_formula | \"0.5*x^2\" | valence | 2\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    "  \"HO\" | 0\n",
    "%\n",
    "\n",
    "ExtraStates = 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "Eigenvalues:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "run.default.scf.eigenvalues()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "If we also want to plot, say, the wave-function, at the end of the calculation, we have to tell **Octopus** to give us this wave-function and how it should do this. We just include\n",
    "\n",
    " %[Output](https://www.octopus-code.org/documentation//13/variables/output/output) <br>\n",
    "    wfs <br>\n",
    " % <br>\n",
    " [OutputFormat](https://www.octopus-code.org/documentation//13/variables/output/outputformat) = axis_x"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "The first line tells **Octopus** to give out the wave-functions and the second line says it should do so along the x-axis. We can also select the wave-functions we would like as output, for example the first and second, the fourth and the sixth. (If you don’t specify anything **Octopus** will give them all.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    " [OutputWfsNumber](https://www.octopus-code.org/documentation//13/variables/output/outputwfsnumber) = \"1-2,4,6\"<br>\n",
    " **Octopus** will store the wave-functions in the same folder static where the info file is, under a meaningful name. They are stored as pairs of the ‘‘x’'-coordinate and the value of the wave-function at that position ‘‘x’’. One can easily plot them with python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_unocc.txt'\n",
    "stderr = 'stderr_unocc.txt'\n",
    "\n",
    "FromScratch = yes\n",
    "CalculationMode = unocc\n",
    "\n",
    "Dimensions = 1\n",
    "TheoryLevel = independent_particles\n",
    "\n",
    "Radius = 10\n",
    "Spacing = 0.1\n",
    "\n",
    "%Species\n",
    "  \"HO\" | species_user_defined | potential_formula | \"0.5*x^2\" | valence | 2\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    "  \"HO\" | 0\n",
    "%\n",
    "\n",
    "ExtraStates = 10\n",
    "\n",
    "%Output\n",
    "  wfs\n",
    "%\n",
    "OutputFormat = axis_x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31",
   "metadata": {},
   "source": [
    "**Octopus** will store the wave-functions in the same folder static where the info file is, under a meaningful name. They are stored as pairs of the ‘‘x’'-coordinate and the value of the wave-function at that position ‘‘x’’. One can easily plot them with gnuplot or a similar program."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run()\n",
    "wf = run.default.scf.wf().isel(step=-1)\n",
    "wavefunctions = [\n",
    "    wf.sel(st=1),\n",
    "    wf.sel(st=2),\n",
    "    wf.sel(st=4),\n",
    "    wf.sel(st=6),\n",
    "]\n",
    "\n",
    "for wf in wavefunctions:\n",
    "    plt.plot(wf.x, wf.real, label=wf.name)\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33",
   "metadata": {},
   "source": [
    "It is also possible to extract a couple of other things from **Octopus** like the density or the Kohn-Sham potential."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34",
   "metadata": {},
   "source": [
    "[Go to *2-particle-in-a-box.ipynb*](2-particle-in-a-box.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
