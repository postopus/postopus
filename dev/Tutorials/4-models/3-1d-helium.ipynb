{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# 1D Helium\n",
    "[Link to tutorial](https://octopus-code.org/documentation/13/tutorial/model/1d_helium/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "The next example will be the helium atom in one dimension which also has two electrons, just as we used for the harmonic oscillator. The main difference is that instead of describing two electrons in one dimension we will describe one electron in two dimensions. The calculation in this case is not a DFT one, but an exact solution of the Schrödinger equation -- not an exact solution of the helium atom, however, since it is a one-dimensional model."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "Equivalence between two 1D electrons and one 2D electron\n",
    "To show that we can treat two electrons in one dimension as one electron in two dimensions, lets start by calling x,x,x, and y,y,y, the coordinates of the two electrons. The Hamiltonian would be (note that the usual Coulomb interaction between particles is usually substituted, in 1D models, by the soft Coulomb potential,\n",
    "$u(x)=(1+x^2)^{(-1/2)}$:\n",
    "<br>\n",
    "$\n",
    "\\hat{H} = -\\frac{1}{2}\\frac{\\partial^2}{\\partial x^2}\n",
    "            -\\frac{1}{2}\\frac{\\partial^2}{\\partial y^2}\n",
    "  +\\frac{-2}{\\sqrt{1+x^2}}+\\frac{-2}{\\sqrt{1+y^2}}+\\frac{1}{\\sqrt{1+(x-y)^2}}\n",
    "$\n",
    "\n",
    "Instead of describing two electrons in one dimension, however, we may very well think of one electron in two dimensions,\n",
    "subject to a external potential with precisely the shape given by:\n",
    "$\n",
    "\\frac{-2}{\\sqrt{1+x^2}}+\\frac{-2}{\\sqrt{1+y^2}}+\\frac{1}{\\sqrt{1+(x-y)^2}}\n",
    " $ \n",
    "\n",
    "\n",
    "Since the Hamiltonian is identical, we will get the same result. Whether we regard x and y, as the coordinates of two different particles in one dimension or as the coordinates of the same particle along the two axes in two dimensions is entirely up to us. (This idea can actually be generalized to treat two 2D particles via a 4D simulation in **octopus** too!) Since it is usually easier to treat only one particle, we will solve the one-dimensional helium atom in two dimensions. We will also therefore get a \"two-dimensional wave-function\". In order to plot this wave-function we specify an output plane instead of an axis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import cm\n",
    "import numpy as np\n",
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 3-1d-helium"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 3-1d-helium"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "### Input"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "With the different potential and one more dimension the new input file looks like the following"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "Dimensions = 2\n",
    "TheoryLevel = independent_particles\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "Lsize = 8\n",
    "Spacing = 0.1\n",
    "\n",
    "%Output\n",
    "  wfs | plane_z\n",
    "%\n",
    "\n",
    "%Species\n",
    " \"helium\" | species_user_defined | potential_formula | \"-2/(1+x^2)^(1/2)-2/(1+y^2)^(1/2)+1/(1+(x-y)^2)^(1/2)\" | valence | 1\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    " \"helium\"| 0 | 0\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "For more information on how to write a potential formula expression, see [Input file](https://www.octopus-code.org/documentation//13/manual/basics/input_file).\n",
    "\n",
    "We named the species “helium” instead of “He” because “He” is already the name of a pseudopotential for the actual 3D helium atom.\n",
    "\n",
    "### Running \n",
    "\n",
    "The calculation should converge within 14 iterations. As usual, the results are summarized in the static/info file, where you can find"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat static/info | grep -A 38 \"[*] Theory Level [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "As we are running with non-interacting electrons, the Hartree, exchange and correlation components of the energy are zero. Also the ion-ion term is zero, as we only have one “ion”."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "### Unoccupied States\n",
    "\n",
    "Now you can do just the same thing we did for the [harmonic oscillator](1-1d-harmonic-oscillator.ipynb) and change the unoccupied calculation mode:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "CalculationMode = unocc\n",
    "\n",
    "stdout = 'stdout_unocc.txt'\n",
    "stderr = 'stderr_unocc.txt'\n",
    "\n",
    "Dimensions = 2\n",
    "TheoryLevel = independent_particles\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "Lsize = 8\n",
    "Spacing = 0.1\n",
    "\n",
    "%Output\n",
    "  wfs | plane_z\n",
    "%\n",
    "OutputWfsNumber = \"1-4,6\"\n",
    "\n",
    "%Species\n",
    " \"helium\" | species_user_defined | potential_formula | \"-2/(1+x^2)^(1/2)-2/(1+y^2)^(1/2)+1/(1+(x-y)^2)^(1/2)\" | valence | 1\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    " \"helium\"| 0 | 0\n",
    "%\n",
    "\n",
    "ExtraStates = 5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "We have added extra states and also restricted the wavefunctions to plot ([OutputWfsNumber]() = \"1-4,6\").\n",
    "\n",
    "The results of this calculation can be found in the file static/eigenvalues . In this case it looks like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "run.default.scf.eigenvalues"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "Apart from the eigenvalues and occupation numbers we asked Octopus to output the wave-functions. To plot them, we will use postopus."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "wf = run.default.scf.wf().squeeze()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "wf.sel(st=1, drop=True).plot.surface(cmap=cm.coolwarm);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "wf.sel(st=2, drop=True).plot.surface(cmap=cm.coolwarm);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "wf.sel(st=3, drop=True).plot.surface(cmap=cm.coolwarm);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": [
    "### Exercises\n",
    "\n",
    "* Calculate the helium atom in 1D, assuming that the 2 electrons of helium do not interact using [Dimensions](https://octopus-code.org/documentation//13/variables/system/dimensions) = 1. Can you justify the differences?\n",
    "\n",
    "* See how the results change when you change the interaction. Often one models the Coulomb interaction by $1/\\sqrt{a^2+r^2}\\,$, and fits the parameter $a\\,$ to reproduce some required property.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "[Go to *4-e-H-scattering.ipynb*](4-e-H-scattering.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  },
  "nbdime-conflicts": {
   "local_diff": [
    {
     "diff": [
      {
       "diff": [
        {
         "diff": [
          {
           "key": 5,
           "op": "addrange",
           "valuelist": "8"
          },
          {
           "key": 5,
           "length": 1,
           "op": "removerange"
          }
         ],
         "key": 0,
         "op": "patch"
        }
       ],
       "key": "version",
       "op": "patch"
      }
     ],
     "key": "language_info",
     "op": "patch"
    }
   ],
   "remote_diff": [
    {
     "diff": [
      {
       "diff": [
        {
         "key": 0,
         "length": 1,
         "op": "removerange"
        }
       ],
       "key": "version",
       "op": "patch"
      }
     ],
     "key": "language_info",
     "op": "patch"
    }
   ]
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
